#!/bin/bash
dpkg -l $1 > /dev/null 2>&1

if [[ $? = 0 ]] 
then
	echo "$1 already installed on your ubuntu machine"

	else

	echo "installing $1 package on your ubuntu machine"
	sudo apt-get update
	sudo apt-get -y install $1
fi
